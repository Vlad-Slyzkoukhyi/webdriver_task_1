﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebDriverTask1
{
    public class PastebinData
    {
        private readonly IWebDriver _driver;

        [FindsBy(How = How.ClassName, Using = "header__btn")]
        private readonly IWebElement? _newPasteButton;
        [FindsBy(How = How.Id, Using = "postform-text")]
        private readonly IWebElement? _postformTextField;
        [FindsBy(How = How.Id, Using = "select2-postform-expiration-container")]
        private readonly IWebElement? _dropDownElement;
        [FindsBy(How = How.CssSelector, Using = "li[class *= 'select2-results__option']")]
        private readonly IList<IWebElement>? _dropDownOptions;
        [FindsBy(How = How.Id, Using = "postform-name")]
        private readonly IWebElement? _titleField;
        [FindsBy(How = How.CssSelector, Using = ".btn.-big")]
        private readonly IWebElement? _submitButtonElement;
        [FindsBy(How = How.ClassName, Using = "info-top")]
        private readonly IWebElement? _existeTitleElement;
        [FindsBy(How = How.ClassName, Using = "li1")]
        private readonly IWebElement? _existeTextElement;
        
        public IWebElement? NewPasteButton => _newPasteButton;
        public IWebElement? PostformTextField => _postformTextField;
        public IWebElement? DropDownElement => _dropDownElement;
        public IList<IWebElement>? DropDownOptions => _dropDownOptions;
        public IWebElement? TitleField => _titleField;
        public IWebElement? SubmitButton => _submitButtonElement;
        public IWebElement? ExisteTitle => _existeTitleElement;
        public IWebElement? ExisteText => _existeTextElement;

        public PastebinData(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void Click(IWebElement? clickableElement)
        {
            clickableElement?.Click();
        }

        public void SendKeys(IWebElement? inputElement, string? inputText)
        {
            inputElement?.SendKeys(inputText);
        }


    }
}
