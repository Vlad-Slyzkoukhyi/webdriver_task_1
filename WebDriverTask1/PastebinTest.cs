using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using WebDriverTask1;

namespace Web_Driver
{
    [TestFixture]
    public class PastebinTest
    {
        private ChromeDriver _driver;
        private const string _url = "https://pastebin.com/";
        private const string _expirationValue = "10 Minutes";
        private const string _inputText = "Hello from WebDriver";
        private const string _inputTitle = "helloweb";

        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
        }

        [Test]
        public void CheckPasteButtonExpirationTime10M()
        {
            PastebinData pb = new (_driver);

            _driver.Navigate().GoToUrl(_url);

            pb.Click(pb.NewPasteButton);
            pb.SendKeys(pb.PostformTextField, _inputText);
            ((IJavaScriptExecutor)_driver).ExecuteScript("window.scrollBy(0,500)", "");
            pb.Click(pb.DropDownElement);
            pb.DropDownOptions?.FirstOrDefault(x => x.Text.Equals(_expirationValue))?.Click();
            pb.SendKeys(pb.TitleField, _inputTitle);
            ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].scrollIntoView(true);", pb.SubmitButton);
            pb.Click(pb.SubmitButton);
            Assert.That(pb.ExisteTitle?.Text, Is.EqualTo(_inputTitle), "The created title does not match the expected title.");
            Assert.That(pb.ExisteText?.Text.Trim(), Is.EqualTo(_inputText.Trim()), "The text content of the <div> element does not match the expected text.");
        }

        [TearDown]
        public void Cleanup()
        {
            _driver.Close();
            _driver.Quit();
        }
    }
}
